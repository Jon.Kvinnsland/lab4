package datastructure;

import cellular.CellState;

import java.util.Arrays;

public class CellGrid implements IGrid {
    private int rows;
    private int columns;
    private CellState[][] cellGrid;

    public CellGrid(int rows, int columns, CellState initialState) {
        if(rows<=0 || columns<=0)
            throw new IllegalArgumentException();

        this.rows = rows;
        this.columns = columns;

        this.cellGrid = new CellState[rows][columns];
        for(int row=0; row < rows; row++){
            for(int cell=0; cell < columns; cell++){
                cellGrid[row][cell] = initialState;
            }
        }

        /*
        System.out.println(Arrays.deepToString(arr2d));

        System.out.println(arr2d.length);
        System.out.println(arr2d[0].length);

        for (int[] row: arr2d) {
            System.out.println(Arrays.toString(row));

        arr2d[2] = new int[3];

         */
        }




    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if(row<0 || row>=numRows() || column<0 || column>=numColumns())
            throw new IndexOutOfBoundsException();

        cellGrid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        if(row<0 || row>=numRows() || column<0 || column>=numColumns())
            throw new IndexOutOfBoundsException();
        return cellGrid[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        CellGrid cellGridCopy = new CellGrid(this.rows, this.columns, null);

        for(int row=0; row < this.rows; row++) {
            for (int cell = 0; cell < this.columns; cell++) {
                cellGridCopy.set(row, cell, this.get(row, cell));
            }
        }
        return cellGridCopy;
    }
    
}
